<?php

use PHPUnit\Framework\TestCase;
use Spliter\Spliter;

final class OrderTest extends TestCase
{
    public function testIsRegularOrder()
    {
        $this->assertEquals('123123', (new Spliter())->order('123123')->orderId);
    }

    public function testHasPart()
    {
        $this->assertEquals('PC', (new Spliter())->order('123123_PC')->partReference);
    }

    public function testHasReprint()
    {
        $this->assertEquals('RP1', (new Spliter())->order('123123_RP1')->reprintReference);
    }

    public function testHasPartReprint()
    {
        $this->assertEquals('RP1', (new Spliter())->order('123123_RP1_PM')->reprintReference);
        $this->assertEquals('PM', (new Spliter())->order('123123_RP1_PM')->partReference);
    }

    public function testHasReference()
    {
        $this->assertEquals('RP1_PC', (new Spliter())->order('123123_RP1_PC')->reference);
    }

    public function testWhatsVersion()
    {
        $this->assertEquals('1', (new Spliter())->order('123123_1')->version);
    }
}
