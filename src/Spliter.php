<?php

namespace Spliter;

/**
 * Spliter
 */
class Spliter
{

    /**
     * Splits Order Id into yout parts
     *
     * @param string $id
     * @return Order
     */
    public function order(string $id): Order
    {
        return new Order($id);
    }
}