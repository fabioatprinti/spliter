<?php

namespace Spliter;

/**
 * Order
 */
class Order
{

    /**
     * Complete Order Id
     *
     * @var string
     */
    public $complete = '';

    /**
     * Only Order Id wihtout Reference
     * eg. PC/M, RP1 or version
     *
     * @var string
     */
    public $orderId = '';

    /**
     * Refence Part of an Order
     * eg. PC or PM
     *
     * @var string
     */
    public $partReference = '';

    /**
     * Just the Reference :)
     * eg. PC/M, RP1
     *
     * @var string
     */
    public $reference = '';

    /**
     * Reprint Reference
     * eg. RP1 or RP2 ...
     *
     * @var string
     */
    public $reprintReference = '';

    /**
     * Version of delivery
     * eg. 1, 2 or 3 ...
     *
     * @var integer
     */
    public $version = 0;


    /**
     * Create a new Split\Order instance
     *
     * @param string $fullId
     */
    public function __construct(string $fullId)
    {
        $this->sectioning($fullId);
    }

    /**
     * Partitioning OrderId into parts
     *
     * @param string $fullId
     * @return void
     */
    private function sectioning(string $fullId)
    {
        $this->complete = $fullId;
        $this->orderId = $fullId;

        $position1 = strpos($fullId, '_');
        if ($position1) {
            $this->orderId = substr($fullId, 0, $position1);
        }

        if (preg_match("/_[0-9]+$/", $fullId, $matches)) {
            $this->version = substr($matches[0], 1);
        }

        if (preg_match("/(_P[a-zA-Z]+)+/i", $fullId, $matches)) {
            $this->partReference = substr($matches[0], 1);
        }

        if (preg_match("/(_RP[0-9]+)+/i", $fullId, $matches)) {
            $this->reprintReference = substr($matches[0], 1);
        }

        if (preg_match("/(_\w*)/", $fullId, $matches)) {
            $this->reference = substr($matches[0], 1);
            if ($this->version !== 0) {
                $position2 = strrpos($this->reference, '_');
                $this->reference = substr($this->reference, 0, $position2);
            }
        }
    }
}